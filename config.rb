require 'zen-grids'
require 'breakpoint'
require 'sass-globbing'
require 'hamburgers'

sass_dir        = "src/sass"
css_dir         = "css"
images_dir      = "images"
javascripts_dir = "js"

add_import_path "../wwuzen/src/sass/modules"

relative_assets = true
