(function ($, Drupal, window, document, undefined) {

  'use strict';

  Drupal.behaviors.toggleMainMenu = {

    attach: function (context) {
      var $main_menu_toggle = $('#main-menu-toggle', context);
      var $main_menu = $('#main-menu .menu-name-main-menu > .menu', context);

      $main_menu_toggle.click(function () {
        $main_menu.stop(true, false);

        if ($main_menu.is(':visible')) {
          $main_menu_toggle.removeClass('is-active').attr('aria-pressed', 'false');
          $main_menu.css('display', 'block').slideUp();
        }
        else {
          $main_menu_toggle.addClass('is-active').attr('aria-pressed', 'true');
          $main_menu.slideDown({
            'complete': function () {
              $main_menu.css('display', 'table');
            }
          });
        }
      });
    }

  };

})(jQuery, Drupal, this, this.document);
