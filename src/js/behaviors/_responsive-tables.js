(function ($, Drupal, window, document, undefined) {

  'use strict';

  Drupal.behaviors.responsiveTables = {

    attach: function (context) {
      var $signature_requests_table = $('.view-review-signature-requests .views-table', context);

      $signature_requests_table.cardtable();
    }

  };

})(jQuery, Drupal, this, this.document);
