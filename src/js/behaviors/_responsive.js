(function ($, enquire, Drupal, window, document, undefined) {

  'use strict';

  Drupal.behaviors.responsive = {

    attach: function (context) {
      var $main_menu_toggle = $('#main-menu-toggle', context);
      var $main_menu = $('#main-menu .menu-name-main-menu > .menu', context);

      enquire.register('all and (min-width: 800px)', {

        match: function () {
          $main_menu.removeAttr('style');
          $main_menu.css('display', 'table');
        },

        unmatch: function () {
          $main_menu_toggle.removeClass('is-active');
          $main_menu_toggle.attr('aria-pressed', 'false');
          $main_menu.css('display', 'none');
        }

      });
    }

  };

})(jQuery, enquire, Drupal, this, this.document);
