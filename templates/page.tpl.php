<?php

/**
 * @file
 * Customized version of Zen page template file.
 */
?>
<div class="page">

  <header class="site-header-wrapper" aria-label="Site Header">
    <div class="center-content">
      <div class="site-header">

        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
          <?php if ($site_name): ?>
          <span class="site-name" aria-label="Site Name"><?php print $site_name; ?></span>
          <?php endif; ?>
        </a>

        <button id="main-menu-toggle" class="main-menu-toggle hamburger hamburger--slider" aria-pressed="false" aria-label="Toggle Main Menu">
          <span class="hamburger-box">
            <span class="hamburger-inner">Toggle Main Menu</span>
          </span>
        </button>

        <nav id="main-menu" class="main-menu" aria-label="Main Menu">
          <?php print render($page['navigation']); ?>
        </nav>

      </div>
    </div>
  </header>

  <main aria-label="Main Content">
    <div class="center-content">

      <header class="page-title" aria-label="Content Title">
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($page['help']); ?>
      </header>

      <section class="content column" aria-label="Content Body">
        <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>

        <?php print render($page['content']); ?>
      </section>

    </div>
  </main>

</div>

<footer aria-label="Site Footer">
  <div class="footer-wrapper center-content">

    <div class="footer-left">
      <?php print render($page['footer_left']); ?>
    </div>

    <div class="footer-center">
      <?php print render($page['footer_center']); ?>

      <div class="western-privacy-statement">
        <a href="http://www.wwu.edu/privacy/">Website Privacy Statement</a>
      </div>
    </div>

    <div class="footer-right">
      <h2><a href="http://www.wwu.edu">Western Washington University</a></h2>

      <div class="western-contact-info">
        <p class="western-address">516 High Street<br> Bellingham, WA 98225</p>
        <p class="western-telephone"><a href="tel:3606503000">(360) 650-3000</a></p>
        <p class="western-contact"><a href="http://www.wwu.edu/wwucontact/">Contact Western</a></p>
      </div>

      <?php print render($page['footer_right']); ?>
    </div>

  </div>
</footer>
